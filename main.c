/*
 * File:   main.c
 * Author: rbeal
 *
 * Created on December 30, 2017, 9:30 PM
 */

#include <xc.h>
#include "types.h"
#include "ios.h"
#include "soft_timer.h"
#include "timer.h"

void ioInit(void);

int main(void) {
    
    eLedState truc;
    u8 i;
    
    ioInit();
    timer_1_init();
    soft_timer_init();
    
    ios_led_init();
    ios_button_init();
    
    ios_led_setState(LED_1, LED_BLINK);
    
    for (;;) {
        
        ios_led_machine();
        ios_button_machine();
        
        
        if (ios_button_getState(BUTTON_4) == BUTTON_PRESSED)
            asm("reset"); // For freestyle reset button
        
        if (ios_button_getState(BUTTON_3) == BUTTON_PRESSED) {
            
            truc = LED_ON;
            
            for (i=0; i<10; i++)
                ios_led_setState(i, truc);
        }
        
        if (ios_button_getState(BUTTON_2) == BUTTON_PRESSED) {
            
            truc = LED_OFF;
            
            for (i=0; i<10; i++)
                ios_led_setState(i, truc);
        }
        
        if (ios_button_getState(BUTTON_1) == BUTTON_PRESSED) {
            
            truc = LED_BLINK;
            
            for (i=0; i<10; i++)
                ios_led_setState(i, truc);
        }
        
    }
    
    return 1;
}


void ioInit(void) {
    
    // Disable alls Analog input
    ANSA = 0;
    ANSB = 0;
    ANSC = 0;
}
